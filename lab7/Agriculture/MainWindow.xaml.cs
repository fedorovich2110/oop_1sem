﻿using System;
using System.Windows;
using System.Windows.Controls;
using AgriculturalProducts;

namespace Agriculture
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }
        WorkProducts products = new WorkProducts();
        bool condition;
        double[] price =
        {
            1.7,
            2,
            1.8,
            1.5,
            1.1,
            2.2,
            3,
            4,
            5,
            2.5
        };
        string type;
        string view;
        private void List1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch(List1.SelectedIndex)
            {
                case 0:
                    view = "картофель";
                    break;
                case 1:
                    view = "свекла";
                    break;
                case 2:
                    view = "огурец";
                    break;
                case 3:
                    view = "помидор";
                    break;
                case 4:
                    view = "яблоко";
                    break;
                case 5:
                    view = "груша";
                    break;
                case 6:
                    view = "апельсин";
                    break;
                case 7:
                    view = "вишня";
                    break;
                case 8:
                    view = "черника";
                    break;
                case 9:
                    view = "малина";
                    break;
            }
        }
        private string TypeDefinition()
        {
            if((List1.SelectedIndex >= 0) && (List1.SelectedIndex <= 3))
            {
                type = "овощ";
            }
            else if((List1.SelectedIndex >= 4) && (List1.SelectedIndex <= 6))
            {
                type = "фрукт";
            }
            else
            {
                type = "ягода";
            }
            return type;
        }
        private void List_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (List.SelectedIndex == 0)
             {
                 condition = true;
             }
             else if (List.SelectedIndex == 1)
             {
                 condition = false;
             }
        }
        private void Accounting_Click(object sender, RoutedEventArgs e)
        {
            products.AddNewMemo(view, TypeDefinition(), price[List1.SelectedIndex], condition);
        }

        private void EcoProduction_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = products.AverageCostEnvironmental(view);
        }

        private void NonProduction_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = products.AverageCostNonEnvironmental(view);
        }

        private void Quantity_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = products.NumberItems();
        }

        private void Output_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = products.ToString();
        }

    }
}
