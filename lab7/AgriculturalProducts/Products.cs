﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgriculturalProducts
{
    class Products
    {
        public string View { get; private set; }
        double price;
        public bool Condition { get; private set; }
        public Products(string view, double price, bool condition)
        {
            View = view;
            this.price = price;
            Condition = condition;
        }
        public Products() { }
        public double IncreaseCost()
        {
            if (Condition == true)
            {
                return price * 1.25;
            }
            return price;
        }
        public override string ToString()
        {
            if (Condition)
            {
                return "Вид: " + View + "; цена: " + IncreaseCost() + ";\n" + "продукция выращена по экологическим усл.";
            }
            else
            {
                return "Вид: " + View + "; цена: " + IncreaseCost() + ";\n" + "продукция выращена по неэкологическим усл.";
            }
        }
    }
}
