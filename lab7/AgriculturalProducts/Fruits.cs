﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgriculturalProducts
{
    class Fruits : Products
    {
        public Fruits(string view, double price, bool condition) : base(view, price, condition)
        {
        }
        public override string ToString()
        {
            return "Фрукт\t" + base.ToString();
        }
    }
}
