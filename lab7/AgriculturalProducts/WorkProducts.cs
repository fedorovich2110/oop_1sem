﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgriculturalProducts
{
    /// <summary>
    /// Этот класс предназначен для работы с родительскими и дочерними классами.
    /// </summary>
    public class WorkProducts
    {
        Products[] products;
        int amountProducts;
        /// <summary>
        /// Конструктор.
        /// </summary>
        public WorkProducts()
        {
            products = new Products[1000];
            amountProducts = 0;
        }
        /// <summary>
        /// Этот метод добавляет новую запись в массив.
        /// </summary>
        /// <param name="view">Строковая переменная.</param>
        /// <param name="type">Строковая переменная.</param>
        /// <param name="price">Двойная переменная.</param>
        /// <param name="condition">Переменная bool.</param>
        public void AddNewMemo(string view, string type, double price, bool condition)
        {
            if(type == "овощ")
            {
                products[amountProducts] = new Vegetables(view, price, condition);
            }
            else if (type == "фрукт")
            {
                products[amountProducts] = new Fruits(view, price, condition);
            }
            else
            {
                products[amountProducts] = new Berries(view, price, condition);
            }
            amountProducts++;
        }
        /// <summary>
        /// Этот метод вычисляет среднюю стоимость окружающей среды определенного вида продукции.
        /// </summary>
        /// <param name="view">строковая переменная.</param>
        /// <returns>средняя стоимость.</returns>
        public string AverageCostEnvironmental(string view)
        {
            double cost = 0;
            int amount = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((products[i].Condition) && (products[i].View == view))
                {
                    cost += products[i].IncreaseCost();
                    amount++;
                }
            }
            return "Ср. цена экологической продукции: " + cost / (amount != 0 ? amount : 1) + "\n";
        }
        /// <summary>
        /// Этот метод вычисляет среднюю стоимость неэквивалентного определенного вида продукции.
        /// </summary>
        /// <param name="view">строковая переменная.</param>
        /// <returns>средняя стоимость.</returns>
        public string AverageCostNonEnvironmental(string view)
        {
            double cost = 0;
            int amount = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((!products[i].Condition) && (products[i].View == view))
                {
                    cost += products[i].IncreaseCost();
                    amount++;
                }
            }
            return "Ср. цена неэкологической продукции: " + cost / (amount != 0 ? amount : 1);
        }
        /// <summary>
        /// Этот метод подсчитывает количество фруктов, овощей, ягод.
        /// </summary>
        /// <returns>Количество фруктов, овощей, ягод</returns>
        public string NumberItems()
        {
            int fruit = 0;
            int vegetable = 0;
            int berry = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((!products[i].Condition))
                {
                    if (products[i] is Fruits)
                    {
                        fruit++;
                    }
                    else if (products[i] is Vegetables)
                    {
                        vegetable++;
                    }
                    else if (products[i] is Berries)
                    {
                        berry++;
                    }
                }
            }
            return "Кол-во фруктов: " + fruit + "\n" + "Кол-во овощей: " + vegetable + "\n" + "Кол-во ягод: " + berry + "\n";
        }
        /// <summary>
        /// Переопределяет метод ToString для вывода всех записей массива.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string answer = "";
            for (int i = 0; i < amountProducts; i++)
            {
                answer += products[i].ToString() + "\n";
            }
            return answer;
        }
    }
}
