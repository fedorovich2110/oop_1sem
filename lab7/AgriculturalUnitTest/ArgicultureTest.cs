﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AgriculturalProducts;

namespace AgriculturalUnitTest
{
    [TestClass]
    public class ArgicultureTest
    {
        [TestMethod]
        public void AverageCostEnvironmental1()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("томат", "овощь", 0.63, false);
            plants.AddNewMemo("капуста", "овощь", 0.85, true);
            plants.AddNewMemo("огурец", "овощь", 1, true);
            plants.AddNewMemo("картофель", "овощь", 0.25, true);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена экологической продукции: 1,25\n";
            //Assert
            Assert.AreEqual(plants.AverageCostEnvironmental("огурец"), assert);
        }
        [TestMethod]
        public void AverageCostEnvironmental2()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("томат", "овощь", 0.63, false);
            plants.AddNewMemo("капуста", "овощь", 0.85, false);
            plants.AddNewMemo("огурец", "овощь", 1, false);
            plants.AddNewMemo("картофель", "овощь", 0.25, false);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена экологической продукции: 0\n";
            //Assert
            Assert.AreEqual(plants.AverageCostEnvironmental("черника"), assert);
        }
        [TestMethod]
        public void AverageCostEnvironmental3()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("смородина", "ягода", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("черника", "ягода", 1, true);
            plants.AddNewMemo("манго", "фрукт", 0.25, true);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена экологической продукции: 0\n";
            //Assert
            Assert.AreEqual(plants.AverageCostEnvironmental("вишня"), assert);
        }
        [TestMethod]
        public void AverageCostNonEnvironmental1()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("смородина", "ягода", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("черника", "ягода", 1, true);
            plants.AddNewMemo("манго", "фрукт", 0.25, false);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена неэкологической продукции: 0,25";
            //Assert
            Assert.AreEqual(plants.AverageCostNonEnvironmental("манго"), assert);
        }
        [TestMethod]
        public void AverageCostNonEnvironmental2()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("смородина", "ягода", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("черника", "ягода", 1, true);
            plants.AddNewMemo("манго", "фрукт", 0.25, true);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена неэкологической продукции: 1,15";
            //Assert
            Assert.AreEqual(plants.AverageCostNonEnvironmental("вишня"), assert);
        }
        [TestMethod]
        public void AverageCostNonEnvironmental3()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("смородина", "ягода", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("черника", "ягода", 1, true);
            plants.AddNewMemo("манго", "фрукт", 0.25, true);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Ср. цена неэкологической продукции: 0";
            //Assert
            Assert.AreEqual(plants.AverageCostNonEnvironmental("банан"), assert);
        }
        [TestMethod]
        public void NumberItems1()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("банан", "фрукт", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("ананас", "фрукт", 1, true);
            plants.AddNewMemo("манго", "фрукт", 0.25, true);
            plants.AddNewMemo("груша", "фрукт", 1.15, false);
            //Act
            string assert = "Кол-во фруктов: " + 2 + "\n" + "Кол-во овощей: " + 0 + "\n" + "Кол-во ягод: " + 0 + "\n";
            //Assert
            Assert.AreEqual(plants.NumberItems(), assert);
        }
        [TestMethod]
        public void NumberItems2()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("томат", "овощ", 0.63, false);
            plants.AddNewMemo("капуста", "овощ", 0.85, true);
            plants.AddNewMemo("огурец", "овощ", 1, true);
            plants.AddNewMemo("картофель", "овощ", 0.25, true);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Кол-во фруктов: " + 0 + "\n" + "Кол-во овощей: " + 1 + "\n" + "Кол-во ягод: " + 1 + "\n";
            //Assert
            Assert.AreEqual(plants.NumberItems(), assert);
        }
        [TestMethod]
        public void NumberItems3()
        {
            //Arrange
            WorkProducts plants = new WorkProducts();
            plants.AddNewMemo("картофель", "овощ", 0.63, false);
            plants.AddNewMemo("груша", "фрукт", 0.85, true);
            plants.AddNewMemo("черника", "ягода", 1, false);
            plants.AddNewMemo("манго", "фрукт", 0.25, false);
            plants.AddNewMemo("вишня", "ягода", 1.15, false);
            //Act
            string assert = "Кол-во фруктов: " + 1 + "\n" + "Кол-во овощей: " + 1 + "\n" + "Кол-во ягод: " + 2 + "\n";
            //Assert
            Assert.AreEqual(plants.NumberItems(), assert);
        }
    }
}
