﻿namespace HumanWFA
{
    partial class HumanForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listForOutput = new System.Windows.Forms.ListView();
            this.output = new System.Windows.Forms.Button();
            this.sorting = new System.Windows.Forms.Button();
            this.load = new System.Windows.Forms.Button();
            this.grades = new System.Windows.Forms.Button();
            this.subjects = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listForOutput
            // 
            this.listForOutput.BackColor = System.Drawing.SystemColors.Menu;
            this.listForOutput.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listForOutput.HideSelection = false;
            this.listForOutput.Location = new System.Drawing.Point(101, 3);
            this.listForOutput.MaximumSize = new System.Drawing.Size(600, 600);
            this.listForOutput.MinimumSize = new System.Drawing.Size(200, 200);
            this.listForOutput.Name = "listForOutput";
            this.listForOutput.Scrollable = false;
            this.listForOutput.Size = new System.Drawing.Size(600, 359);
            this.listForOutput.TabIndex = 0;
            this.listForOutput.TileSize = new System.Drawing.Size(500, 20);
            this.listForOutput.UseCompatibleStateImageBehavior = false;
            this.listForOutput.View = System.Windows.Forms.View.Tile;
            // 
            // output
            // 
            this.output.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.output.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.output.Location = new System.Drawing.Point(16, 381);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(142, 44);
            this.output.TabIndex = 1;
            this.output.Text = "ВЫВОД";
            this.output.UseVisualStyleBackColor = false;
            this.output.Click += new System.EventHandler(this.Output_Click);
            // 
            // sorting
            // 
            this.sorting.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.sorting.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sorting.Location = new System.Drawing.Point(169, 374);
            this.sorting.Name = "sorting";
            this.sorting.Size = new System.Drawing.Size(142, 44);
            this.sorting.TabIndex = 2;
            this.sorting.Text = "СОРТИРОВКА";
            this.sorting.UseVisualStyleBackColor = false;
            this.sorting.Click += new System.EventHandler(this.Sorting_Click);
            // 
            // load
            // 
            this.load.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.load.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.load.Location = new System.Drawing.Point(488, 368);
            this.load.Name = "load";
            this.load.Size = new System.Drawing.Size(142, 53);
            this.load.TabIndex = 3;
            this.load.Text = "НАГРУЗКА УЧИТЕЛЯ";
            this.load.UseVisualStyleBackColor = false;
            this.load.Click += new System.EventHandler(this.Load_Click);
            // 
            // grades
            // 
            this.grades.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.grades.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grades.Location = new System.Drawing.Point(650, 368);
            this.grades.Name = "grades";
            this.grades.Size = new System.Drawing.Size(142, 57);
            this.grades.TabIndex = 4;
            this.grades.Text = "СТУДЕНТЫ (> 1-ой 9)";
            this.grades.UseVisualStyleBackColor = false;
            this.grades.Click += new System.EventHandler(this.Grades_Click);
            // 
            // subjects
            // 
            this.subjects.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.subjects.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.subjects.FormattingEnabled = true;
            this.subjects.ItemHeight = 22;
            this.subjects.Items.AddRange(new object[] {
            "ОАиП",
            "Математика",
            "Физика"});
            this.subjects.Location = new System.Drawing.Point(330, 368);
            this.subjects.Name = "subjects";
            this.subjects.Size = new System.Drawing.Size(142, 70);
            this.subjects.TabIndex = 5;
            // 
            // HumanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.subjects);
            this.Controls.Add(this.grades);
            this.Controls.Add(this.load);
            this.Controls.Add(this.sorting);
            this.Controls.Add(this.output);
            this.Controls.Add(this.listForOutput);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "HumanForm";
            this.Text = "Univer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listForOutput;
        private System.Windows.Forms.Button output;
        private System.Windows.Forms.Button sorting;
        private System.Windows.Forms.Button load;
        private System.Windows.Forms.Button grades;
        private System.Windows.Forms.ListBox subjects;
    }
}

