﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using HumanLibrary;

namespace HumanWFA
{
    public partial class HumanForm : Form
    {
        public HumanForm()
        {
            InitializeComponent();
        }
        WorkHuman human = new WorkHuman();
        private void PaintedRed()
        {
            bool[] index;
            string[] humans;
            humans = human.StudentRed(out index);
            for (int i = 0; i < index.Length; i++)
            {
                listForOutput.Items.Add(humans[i]);
            }
            for (int i = 0; i < index.Length; i++)
            {
                if (index[i])
                {
                    listForOutput.Items[i].ForeColor = Color.Red;
                }
                else
                {
                    listForOutput.Items[i].ForeColor = Color.Black;
                }
            }
        }
        private string Subjects_SelectedIndexChanged()
        {
            string subject = "";
            if (subjects.SelectedIndex == 0)
            {
                subject = "ОАиП";
            }
            else if (subjects.SelectedIndex == 1)
            {
                subject = "Математика";
            }
            else if (subjects.SelectedIndex == 2)
            {
                subject = "Физика";
            }
            return subject;
        }
        private void Output_Click(object sender, EventArgs e)
        {
            StreamReader file = new StreamReader(@"C:\Users\User\Desktop\chelovek.txt", Encoding.Default);
            string text = file.ReadToEnd();
            human.WordParser(text);
            listForOutput.Items.Clear();
            PaintedRed();
        }
        
        private void Sorting_Click(object sender, EventArgs e)
        {
            listForOutput.Items.Clear();
            human.SortSurname();
            string[] h = human.FormString();
            for (int i = 0; i < h.Length; i++)
            {
                listForOutput.Items.Add(h[i]);
            }
        }
        private void Load_Click(object sender, EventArgs e)
        {
            listForOutput.Items.Clear();
            listForOutput.Items.Add("Нагрузка преподавателей старше 40 лет по заданному предмету: \n");
            listForOutput.Items.Add($"{human.MaxLoad(Subjects_SelectedIndexChanged())}");
        }

        private void Grades_Click(object sender, EventArgs e)
        {
            listForOutput.Items.Clear();
            listForOutput.Items.Add("Студенты, которые имеют больше одной 9: \n");
            string[] output = human.InfoStudent();
            for (int i = 0; i < output.Length; i++)
            {
                if (output[i] != null)
                    listForOutput.Items.Add(output[i]);
            }
        }
    }
}
