﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HumanLibrary;

namespace HumanTest
{
    [TestClass]
    public class HumansUnitTest
    {
        [TestMethod]
        public void MaxLoad1()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Иванов 1989 студент 5 6 3 4\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1967 преподаватель 54 68 34\r\n" + "Ивашин 2002 преподаватель 78 15 49\r\n";
            humans.WordParser(text);
            //Act
            int assert = 54;
            //Assert
            Assert.AreEqual(humans.MaxLoad("ОАиП"), assert);
        }
        [TestMethod]
        public void MaxLoad2()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Иванов 1959 преподаватель 78 19 81\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1967 преподаватель 54 68 34\r\n" + "Ивашин 2002 преподаватель 78 15 49\r\n";
            humans.WordParser(text);
            //Act
            int assert = 87;
            //Assert
            Assert.AreEqual(humans.MaxLoad("Математика"), assert);
        }
        [TestMethod]
        public void MaxLoad3()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Петров 1978 бомж\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1978 бомж\r\n" + "Ивашин 1978 ученик\r\n";
            humans.WordParser(text);
            //Act
            int assert = 0;
            //Assert
            Assert.AreEqual(humans.MaxLoad("Математика"), assert);
        }
        [TestMethod]
        public void InfoStudent1()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Иванов 1989 студент 5 9 9 4\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1967 студент 5 2 9 4\r\n" + "Ивашин 1989 преподаватель 78 15 49\r\n";
            humans.WordParser(text);
            //Act
            string[] assert = { "Фамилия: Иванов; статус: студент; год рождения: 1989; сведения: 6\n", null, null, null };
            //Assert
            CollectionAssert.AreEqual(humans.InfoStudent(), assert);
        }
        [TestMethod]
        public void InfoStudent2()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Иванов 1989 студент 5 9 9 4\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1967 студент 8 6 9 4\r\n" + "Ивашин 2002 студент 6 9 9 8\r\n";
            humans.WordParser(text);
            //Act
            string[] assert = { "Фамилия: Иванов; статус: студент; год рождения: 1989; сведения: 6\n", null, null, "Фамилия: Ивашин; статус: студент; год рождения: 2002; сведения: 8\n" };
            //Assert
            CollectionAssert.AreEqual(humans.InfoStudent(), assert);
        }
        [TestMethod]
        public void InfoStudent3()
        {
            //Arrange
            WorkHuman humans = new WorkHuman();
            string text = "Иванов 1989 студент 5 2 9 4\r\n" + "Петров 1978 бомж\r\n" + "Сидоров 1967 студент 8 6 9 4\r\n" + "Ивашин 2002 студент 8 8 8 8\r\n";
            humans.WordParser(text);
            //Act
            string[] assert = { null, null, null, null };
            //Assert
            CollectionAssert.AreEqual(humans.InfoStudent(), assert);
        }
    }
}
