﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// студенчиский класс людей.
    /// </summary>
    public class Student : Human
    {
        /// <summary>
        /// Это свойство отвечает за множество оценок учащихся.
        /// </summary>
        public int[] Grades { get; private set; }
        /// <summary>
        /// Это переопределенный конструктор для ученика дочернего класса.
        /// </summary>
        /// <param name="surname">Это строковая переменная.</param>
        /// <param name="yearBirth">Это переменная int.</param>
        /// <param name="stat">Это строковая переменная.</param>
        /// <param name="grades">массив.</param>
        public Student(string surname, int yearBirth, string stat, int[] grades)
            : base(surname, yearBirth, stat)
        {
            Grades = grades;
        }
        /// <summary>
        /// Это переопределенный метод, который подсчитывает средний балл за сеанс.
        /// </summary>
        /// <returns>Общая годовая нагрузка.</returns>
        public override int Svedenija()
        {
            int average = 0;
            for (int i = 0; i < Grades.Length; i++)
            {
                average += Grades[i];
            }
            return average / Grades.Length;
        }

        /// <summary>
        /// метод класса Student. 
        /// </summary>
        /// <param name="text">строковая переменная.</param>
        /// <param name="s">Student.</param>
        /// <returns>True, если класс create, и false, если нет.</returns>
        public static bool CheckFotmattString(string text,out Student s)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            int count = 0;
            int[] grade = new int[4];
            Regex surname = new Regex(@"[А-Я][а-я]+");
            words = text.Split(' ');
            if(words[2] == "студент")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    if (Int32.TryParse(words[j], out int result1) && result1 <= 10)
                    {
                        grade[count] = result1;
                        count++;
                    }
                }
                s = new Student(surnames, year, "студент", grade);
                return true;
            }
            else
            {
                s = null;
                return false;
            }
        }
    }
}
