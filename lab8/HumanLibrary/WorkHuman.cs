﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// Это класс для работы с родительскими и дочерними классами.
    /// </summary>
    public class WorkHuman
    {
        Human[] human;
        /// <summary>
        /// Это свойство отвечает за длину массива человека.
        /// </summary>
        public int HumanLength { get; private set; }
        /// <summary>
        /// Конструктор.
        /// </summary>
        public WorkHuman()
        {
            human = new Human[1000];
            HumanLength = 0;
        }
        /// <summary>
        /// Этот метод формирует массив объектов.
        /// </summary>
        /// <param name="text">Это строковая переменная.</param>
        /// <returns>массив Human.</returns>
        public Human[] WordParser(string text)
        {
            string[] separator = { "\r\n" };
            string[] sentences = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            Student s;
            Teacher t;
            Human h;
            HumanLength = sentences.Length;
            for (int i = 0; i < sentences.Length; i++)
            {
                HumanLength = sentences.Length;
                if (Student.CheckFotmattString(sentences[i], out s))
                    human[i] = s;
                else if (Teacher.CheckFotmattString(sentences[i], out t))
                    human[i] = t; 
                else if (Human.CheckFotmattString(sentences[i], out h))
                    human[i] = h;  
            }
            return human;
        }
        /// <summary>
        /// Этот метод учитывает среднюю нагрузку преподавателей старше года по конкретному предмету.
        /// </summary>
        /// <param name="subject">Это строковая переменная.</param>
        /// <returns>Средняя нагрузка учителей.</returns>
        public int MaxLoad(string subject)
        {
            int maxLoad = 0;
            for (int i = 0; i < HumanLength; i++)
            {
                if ((2020 - human[i].YearBirth > 40) && (human[i] is Teacher))
                {
                    Teacher teacher = human[i] as Teacher;
                    maxLoad += teacher[subject];
                }
            }
            return maxLoad;
        }
        /// <summary>
        /// Этот метод ищет студентов старше 19 лет с более чем одним 9.
        /// </summary>
        /// <returns>Множество студентов с более чем одним 9</returns>
        public string[] InfoStudent()
        {
            int amount = 0;
            string[] stringStudent = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                if ((human[i] is Student))
                {
                    Student student = human[i] as Student;
                    for (int j = 0; j < (student.Grades).Length; j++)
                    {
                        if (student.Grades[j] == 9)
                        {
                            amount++;
                        }
                    }
                    if (amount > 1)
                    {
                        stringStudent[i] = student.ToString();
                    }
                    amount = 0;
                }
            }
            return stringStudent;
        }
        /// <summary>
        /// Этот метод формирует массив строк.
        /// </summary>
        /// <returns>Информация о человеке.</returns>
        public string[] FormString()
        {
            string[] answer = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                answer[i] = human[i].ToString() + "\r\n";
            }
            return answer;
        }
        /// <summary>
        ///Сортирует массив людей по фамилии.
        /// </summary>
        public void SortSurname()
        {
            Array.Sort(human, 0, HumanLength);
        }
        /// <summary>
        /// Этот метод ищут студенты старше 19 лет.
        /// </summary>
        /// <param name="index">Это массив переменных bool.</param>
        /// <returns>Массив строк.</returns>
        public string[] StudentRed(out bool[] index)
        {
            index = new bool[HumanLength];
            string[] humanString = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                if ((human[i] is Student) && (2019 - human[i].YearBirth > 19))
                {
                    index[i] = true;
                }
                humanString[i] = human[i].ToString();
            }
            return humanString;
        }
    }
}
