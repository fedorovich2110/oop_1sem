﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// Это родительский класс людей с реализованным интерфейсом.
    /// </summary>
    public class Human : IComparable<Human>
    {
        string surname;

        /// <summary>
        ///Это свойство, ответственная за год рождения.
        /// </summary>
        public int YearBirth { get; private set; }

        string stat;


        /// <summary>
        /// конструктор.
        /// </summary>
        /// <param name="surname">стрроковая переменная</param>
        /// <param name="yearBirth">переменная int.</param>
        /// <param name="stat">Это строковая переменная.</param>
        public Human(string surname, int yearBirth, string stat)
        {
            this.surname = surname;
            YearBirth = yearBirth;
            this.stat = stat;
        }


        /// <summary>
        ///Это метод, который учитывает возраст человека.
        /// </summary>
        /// <returns>Возраст человека.</returns>
        public virtual int Svedenija()
        {
            return 2020 - YearBirth;
        }


        /// <summary>
        /// Метод формирования строки.
        /// </summary>
        /// <returns>Информация о людях.</returns>
        public override string ToString()
        {
            return "Фамилия: " + surname + "; статус: " + stat + "; год рождения: " + YearBirth + "; сведения: " + Svedenija() + "\n";
        }


        /// <summary>
        /// Этот метод предназначен для сравнения текущего объекта с объектом, который передается в качестве параметра.
        /// </summary>
        /// <param name="h">объект Human class.</param>
        /// <remarks>Значение меньше нуля, если текущий объект находится перед объектом параметра, равно нулю, если оба объекта равны и больше нуля, если после объекта параметра.</remarks>
        /// <returns>Переменная int.</returns>
        public int CompareTo(Human h)
        {
            //Human h1 = new Human( "rtfy", 4, "ertdy");
            //Human h2 = new Human( "rtfy", 5, "ertdy");

            //Boolean a = h1.Equals(h2);

            return surname.CompareTo(h.surname);
        }


        /// <summary>
        /// Этот метод формирует класс человека.
        /// </summary>
        /// <param name="text">Это строковая переменная.</param>
        /// <param name="h">Human переменная.</param>
        /// <returns>True, если класс create, и false, если нет.</returns>
        public static bool CheckFotmattString(string text, out Human h)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            string stat = "";
            words = text.Split(' ');
            if(words[2] != "преподаватель" && words[2] != "студент")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    stat = words[2];
                }
                h = new Human(surnames, year, stat);
                return true;
            }
            else
            {
                h = null;
                return false;
            }
        }
    }
}
