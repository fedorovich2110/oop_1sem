﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// Это детский класс людей.
    /// </summary>
    public class Teacher : Human
    {
        /// <summary>
        /// Это свойство отвечает за массив нагрузки учителя.
        /// </summary>
        public int[] Load { get; private set; }
        /// <summary>
        /// Это переопределенный конструктор для детского классного руководителя.
        /// </summary>
        /// <param name="surname">Это строковая переменная.</param>
        /// <param name="yearBirth">Это переменная int.</param>
        /// <param name="stat">Это строковая переменная.</param>
        /// <param name="load">массив.</param>
        public Teacher(string surname, int yearBirth, string stat, int[] load)
            : base(surname, yearBirth, stat)
        {
            Load = load;
        }
        /// <summary>
        /// Это индексатор со строковым типом индекса.
        /// </summary>
        /// <param name="index">Это строковая переменная.</param>
        /// <returns>В зависимости от индекса возвращает элемент массива.</returns>
        public int this[string index]
        {
            get
            {
                switch (index)
                {
                    case "ОАиП": return Load[0];
                    case "Математика": return Load[1];
                    case "Физика": return Load[2];
                    default: return 0;
                }
            }
            set
            {
                switch (index)
                {
                    case "ОАиП":
                        Load[0] = value;
                        break;
                    case "Математика":
                        Load[1] = value;
                        break;
                    case "Физика":
                        Load[2] = value;
                        break;
                }
            }
        }
        /// <summary>
        /// Это переопределенный метод, который учитывает нагрузку учителя.
        /// </summary>
        /// <returns>Общая годовая нагрузка.</returns>
        public override int Svedenija()
        {
            int average = 0;
            for (int i = 0; i < Load.Length; i++)
            {
                average += Load[i];
            }
            return average;
        }
        /// <summary>
        /// метод класса Teacher. 
        /// </summary>
        /// <param name="text">строковая переменная.</param>
        /// <param name="t">Это переменная учителя.</param>
        /// <returns>True если класс create и false если нет.</returns>
        public static bool CheckFotmattString(string text, out Teacher t)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            int count = 0;
            int[] load = new int[3];
            Regex surname = new Regex(@"[А-Я][а-я]+");
            words = text.Split(' ');
            if (words[2] == "преподаватель")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    if (Int32.TryParse(words[j], out int result2) && result2 > 10 && result2 < 200)
                    {
                        load[count] = result2;
                        count++;
                    }
                }
                t = new Teacher(surnames, year, "преподаватель", load);
                return true;
            }
            else
            {
                t = null;
                return false;
            }
        }
    }
}
