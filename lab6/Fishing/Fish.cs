﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Shop;

namespace Fishing
{
    public partial class FishingForm : Form
    {

        public FishingForm()
        {
            InitializeComponent();
            this.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline);
        }

        FishShop fish = new FishShop();

        //Этот метод возвращает массив индексных покупок.
        private int[] CheckedListBox1_SelectedIndexChanged()
        {
            int[] indexGaer = new int[checkedListBoxSnasty.SelectedIndices.Count];
            int i = 0;


            foreach (int indexChecked in checkedListBoxSnasty.CheckedIndices)
            {
                if (i >= indexGaer.Length) return indexGaer;
                indexGaer[i] = indexChecked;
                i++;
            }
            //for (int i = 0; i < checkedListBoxSnasty.SelectedIndices.Count; i++)
            //{
            //    indexGaer[i] = checkedListBoxSnasty.SelectedIndices[i];
            //}



            return indexGaer;
        }

        // покупка 
        private void Button4_Click(object sender, EventArgs e)
        {
            if ((dateTimePicker3.Checked) && (checkedListBoxSnasty.SelectedItems.Count > 0) && (listBoxSeason.SelectedItems.Count > 0))
            {
                
                fish.AddNewMemo(CheckedListBox1_SelectedIndexChanged(), ListBox1_SelectedIndexChanged(), DateTimePicker3_ValueChanged());
                for (int i = 0; i < 7; i++)
                {
                    checkedListBoxSnasty.SetItemCheckState(i, CheckState.Unchecked);
                }
                listBoxWin.ClearSelected();
                listBoxWin.Items.Clear();

                listBoxWin.Items.AddRange(fish.seils());
            }
            else  MessageBox.Show("Вы ввели не все значения!");
        }

        //Этот метод возвращает условие рыбалки (зима/ лето)
        private bool ListBox1_SelectedIndexChanged()
        {
            bool condition = false;
            if (listBoxSeason.SelectedIndex == 0)
            {
                condition = false;
            }
            else if (listBoxSeason.SelectedIndex == 1)
            {
                condition = true;
            }
            return condition;
        }

        //Этот метод возвращает дату покупки.
        private DateTime DateTimePicker3_ValueChanged()
        {
            DateTime date;
            date = dateTimePicker3.Value;
            return date;
        }

        //Этот метод возвращает дату начала периода.
        private DateTime DateTimePicker1_ValueChanged()
        {
            DateTime date = dateTimeStart.Value;
            return date;
        }

        //Этот метод возвращает дату окончания периода.
        private DateTime DateTimePicker2_ValueChanged()
        {
            DateTime date = dateTimeFinish.Value;
            return date;
        }

        // среднее
        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Выберите период!");
            if ((dateTimePicker3.Checked) && (dateTimeStart.Checked))
            {
                textBox1.Text = (fish.AverageAmount(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged()).ToString());
            }
        }


        private void Button2_Click(object sender, EventArgs e)
        {
            if ((dateTimePicker3.Checked) && (dateTimeStart.Checked) && (listBoxSeason.SelectedItems.Count > 0))
                textBox1.Text = (fish.VolumeSales(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged(), ListBox1_SelectedIndexChanged()).ToString());
            else
                MessageBox.Show("Вы ввели не все параметры!");
        }

        // популярный
        private void Button3_Click(object sender, EventArgs e)
        {
            int[] indexArray;
            indexArray = fish.PopularGood(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged());
            if ((dateTimePicker3.Checked) && (dateTimeStart.Checked))
                textBox1.Text = (fish.PopularGoods(indexArray));
            else
                MessageBox.Show("Вы ввели не все параметры!");
        }

    }
}