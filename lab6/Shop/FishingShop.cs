﻿using System;

namespace Shop
{
    /// <summary>
    /// Этот класс работает с подклассом и связанными с ним методами.
    /// </summary>
    public class FishShop
    {
        private class Purchase
        {
            private enum FishingGear
            {
                rod = 177,
                spinning = 150,
                feeder = 136,
                fishLine = 119,
                wobbler = 514,
                mormyshka = 52,
                reel = 542
            };
            // 0 - зима, 1 - лето
            public bool fishCondition;
            public DateTime date;
            public int[] index { get; set; }
            public Purchase(int[] indexFisingGaer, bool fishCondition, DateTime date)
            {
                this.fishCondition = fishCondition;
                this.date = date;
                index = indexFisingGaer;
            }

                    public int CalcCost()
            {
                int cost = 0;
                for (int i = 0; i < index.Length; i++)
                {
                    switch (index[i])
                    {
                        case 0:
                            cost += (int)FishingGear.rod;
                            break;
                        case 1:
                            cost += (int)FishingGear.spinning;
                            break;
                        case 2:
                            cost += (int)FishingGear.feeder;
                            break;
                        case 3:
                            cost += (int)FishingGear.fishLine;
                            break;
                        case 4:
                            cost += (int)FishingGear.wobbler;
                            break;
                        case 5:
                            cost += (int)FishingGear.mormyshka;
                            break;
                        case 6:
                            cost += (int)FishingGear.reel;
                            break;
                    }
                }
                return cost;
            }
        }
        
        private Purchase[] sellings;

        // количество покупок
        int amountSellings;

        /// <summary>
        /// конструктор класса FishShop.
        /// </summary>
        public FishShop()
        {
            sellings = new Purchase[1000];
            amountSellings = 0;
        }


        /// <summary>
        /// Этот метод добавляет новую покупку.
        /// </summary>
        /// <param name="indexFisingGaer">Это индекс int array цены покупки.</param>
        /// <param name="period">Переменная Bool, которая показывает условие catch.</param>
        /// <param name="date">Это переменная DateTime, которая показывает дату покупки.</param>
        public void AddNewMemo(int[] indexFisingGaer, bool period, DateTime date)
        {
            sellings[amountSellings] = new Purchase(indexFisingGaer, period, date);
            amountSellings++;
        }



        public string[] seils()
        {
            string[] str = new String[amountSellings];
            for (int i = 0; i < amountSellings; i++)
            {
                string seson = "";
                if (sellings[i].fishCondition)
                {
                    seson = "лето";
                }
                else seson = "зима";

                string sells = "";
                for (int j = 0; j < (sellings[i].index).Length; j++) {

                    sells += (FishingGoods(sellings[i].index[j]) + "   ");
                }

                str[i] = String.Format("{0}. Условие ловли (сезон): {1}\n" +
                                     "   Дата покупки: {2}\n" +
                                     "   Покупки: {3}\n\n", i, seson, sellings[i].date, sells);
            }
            return str;
        }

        /// <summary>
        /// Этот метод расчета средней стоимости покупок.
        /// </summary>
        /// <param name="beginningPeriod">DateTime переменная.</param>
        /// <param name="endPeriod">DateTime переменная.</param>
        /// <returns></returns>
        public int AverageAmount(DateTime beginningPeriod, DateTime endPeriod)
        {
            int averaeCost = 0;
            int amountPurchese = 0;
            for (int i = 0; i < amountSellings; i++)
            {
                if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                {
                    averaeCost += sellings[i].CalcCost();
                    amountPurchese++;
                }
            }
            return averaeCost / (amountPurchese != 0 ? amountPurchese : 1);
        }

        /// <summary>
        /// Этот метод вычисляет продажи.
        /// </summary>
        /// <param name="beginningPeriod">DateTime переменная.</param>
        /// <param name="endPeriod">DateTime переменная.</param>
        /// <param name="fishCondition">Переменная Bool, которая показывает условие catch.</param>
        /// <returns></returns>
        public int VolumeSales(DateTime beginningPeriod, DateTime endPeriod, bool fishCondition)
        {
            int volumeCost = 0;
            for (int i = 0; i < amountSellings; i++)
            {
                if (!fishCondition.Equals(sellings[i].fishCondition) )
                {
                    if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                    {
                        volumeCost += sellings[i].CalcCost();
                    }
                }
            }
            return volumeCost;

            //int averaeCost = 0;
            //int amountPurchese = 0;
            //for (int i = 0; i < amountSellings; i++)
            //{
            //    if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
            //    {
            //        averaeCost += sellings[i].CalcCost();
            //        amountPurchese++;
            //    }
            //}
            //return averaeCost / (amountPurchese != 0 ? amountPurchese : 1);

        }


        /// <summary>
        /// В зависимости от индекса, этот метод отображает товары.
        /// </summary>
        /// <param name="number">Индекс int.</param>
        /// <returns>Строка с популярным продуктом.</returns>
        private string FishingGoods(int number)
        {
            string popularGood = "";
            switch (number)
            {
                case 0:
                    popularGood = "удочка";
                    break;
                case 1:
                    popularGood = "спиннинг";
                    break;
                case 2:
                    popularGood = "фидер";
                    break;
                case 3:
                    popularGood = "леска";
                    break;
                case 4:
                    popularGood = "воблер";
                    break;
                case 5:
                    popularGood = "мормышка";
                    break;
                case 6:
                    popularGood = "катушка";
                    break;
            }
            return popularGood;
        }

        /// <summary>
        /// Этот метод выполняет поиск по количеству обращений к каждому продукту.
        /// </summary>
        /// <param name="beginningPeriod">DateTime переменная.</param>
        /// <param name="endPeriod">DateTime переменная.</param>
        /// <returns>The array with the number of calls to each product.</returns>
        public int[] PopularGood(DateTime beginningPeriod, DateTime endPeriod)
        {
            //string popularGood = "";
            int[] amount = new int[7];
            for (int i = 0; i < amountSellings; i++)
            {
                if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                {
                    for (int j = 0; j < (sellings[i].index).Length; j++)
                        amount[sellings[i].index[j]]++;
                }
            }
            return amount;
        }

        /// <summary>
        /// Этот метод ищет наиболее популярный продукт.
        /// </summary>
        /// <param name="amount">Переменная массива .</param>
        /// <returns>Строка с самым популярным продуктом.</returns>
        public string PopularGoods(int[] amount)
        {
            int max = 0;
            int maxIndex = 0;
            string popularGood = "";
            bool means = false;
            for (int i = 0; i < 7; i++)
            {
                bool mean = true;
                for (int m = 0; m < 7; m++)
                {
                    int ai = amount[i];
                    int am = amount[m];
                    if ((ai == am) && (i != m))
                        mean = false;
                }
                if (max < amount[i])
                {
                    max = amount[i];
                    maxIndex = i;
                    means = mean;
                }
            }
            if (means)
                popularGood = "Самый популярный товар: " + FishingGoods(maxIndex);
            else
                popularGood = "Нет самого популярного товара.";
            return popularGood;
        }
    }
}