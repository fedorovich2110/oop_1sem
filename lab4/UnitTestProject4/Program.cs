﻿using System;
using ClassString;
using System.Text;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu = 0;
            string data = "";
            StringWork sentence = new StringWork();
            do
            {
                Console.WriteLine(" ");
                Console.WriteLine("*************МЕНЮ*************");
                Console.WriteLine("1. Удалить повторяющиеся слова");
                Console.WriteLine("2. Форма StringBuilder");
                Console.WriteLine("3. Выход");
                Console.WriteLine("******************************");
                Console.Write("Выберите пункт меню: ");
                Int32.TryParse(Console.ReadLine(), out menu);
                switch (menu)
                {
                    case 1:
                        Console.WriteLine("введите предложение с повторяющимися словами:");
                        data = Console.ReadLine();
                        //string data = "В лесу лесу родилась родилась елочка.";
                        sentence = new StringWork(data);
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine(sentence.SplitProposal());
                        break;
                    case 2:
                        Console.WriteLine("Введите имя сайта, дату и время входа в систему.");
                        Console.WriteLine("Например, http://ya.ru 31/01/19 18-30.");
                        //string data = "http://ya.ru 12/01/19 18-30 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
                        data = Console.ReadLine();
                        sentence = new StringWork(data);
                        try
                        {
                            sentence.Checking();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Ошибка: " + e.Message);
                        }
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine(sentence.FormString());
                        break;
                }
            } while (menu != 3);
            Console.ReadKey();
        }
    }
}