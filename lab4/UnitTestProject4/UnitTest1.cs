﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassString;

namespace UnitTestProject4
{
    [TestClass]

    public class UnitTestString
    {
        [TestMethod]

        public void SplitProposal0()
        {
            //Arrange
            string data = "В лесу лесу лесу родилась родилась ёлочка.";
            StringWork stringCheck = new StringWork(data);
            string expected = "В лесу родилась ёлочка. ";
            //Act
            string actual = stringCheck.SplitProposal();
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]

        public void SplitProposal1()
        {
            //Arrange
            string data = "В лесу родилась ёлочка! Он пошел гулять гулять!";
            StringWork stringCheck = new StringWork(data);
            string expected = "В лесу родилась ёлочка. Он пошел гулять. ";
            //Act
            string actual = stringCheck.SplitProposal();
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]

        public void SplitProposal2()
        {
            //Arrange
            string data = "В лес лес лес пошла пошла пошла пошла пошла пошла ворона! Она устала устала устала.";
            StringWork stringCheck = new StringWork(data);
            string expected = "В лес пошла ворона. Она устала. ";
            //Act
            string actual = stringCheck.SplitProposal();
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]

        public void SplitProposal3()
        {
            //Arrange
            string data = "0 0 0 Лес Лес лес лес 1111 1111.";
            StringWork stringCheck = new StringWork(data);
            string expected = "0 лес 1111. ";
            //Act
            string actual = stringCheck.SplitProposal();
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void NullString()
        {
            //Arrange
            string data = "В лесу родилась ёлочка.";
            //Assert
            Assert.IsNotNull(data);
        }

        [TestMethod]

        public void Checking0()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            //Act
            bool actual = stringCheck.Checking();
            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]

        public void Checking1()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30 12/01/19 18-30. http://yaпке.ru. http://yajпа.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            string expected = "Incorrect data entry!";
            //Act
            try
            {
                stringCheck.Checking();
            }
            catch (Exception e)
            {
                //Assert
                Assert.AreEqual(expected, e.Message);
            }
        }

        [TestMethod]

        public void Checking2()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            //Act
            bool actual = stringCheck.Checking();
            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]

        public void Checking3()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30 22/01/19 19-30 17/08/19 20-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 19/11/19 15-40.";
            StringWork stringCheck = new StringWork(data);
            //Act
            bool actual = stringCheck.Checking();
            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]

        public void FormString0()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder("http://ya.ru 12/01/19" + "\n" + "http://yaпке.ru 31/01/19" + "\n" + "http://yajпа.ru 31/01/19 " + "\n");
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]

        public void FormString1()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder("http://ya.ru 12/01/19" + "\n");
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]

        public void FormString2()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://aпке.ru 31/01/19 18-30 31/01/19 18-30 31/01/19 18-30. http://tya.ru 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30 31/01/19 18-30. http://yan.ru 12/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder("http://ya.ru 12/01/19" + "\n" + "http://tya.ru 12/01/19" + "\n" + "http://yan.ru 12/01/19 " + "\n");
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]

        public void FormString3()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://па.ru 31/01/19 18-30 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder("http://ya.ru 12/01/19" + "\n" + "http://yaпке.ru 31/01/19" + "\n" + "http://yajпа.ru 31/01/19 " + "\n");
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]
        public void FormString4()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder(null);
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]

        public void FormString5()
        {
            //Arrange
            string data = "http://ya.ru 12/01/19 18-30. http://gta.ru 31/01/19 18-30. http://wer.ru 31/01/19 18-30. http://top.ru 31/01/19 18-30.";
            StringWork stringCheck = new StringWork(data);
            StringBuilder expected = new StringBuilder("http://ya.ru 12/01/19" + "\n" + "http://gta.ru 31/01/19" + "\n" + "http://wer.ru 31/01/19" + "\n" + "http://top.ru 31/01/19 " + "\n");
            //Act
            StringBuilder actual = stringCheck.FormString();
            //Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }
    }
}