﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace ClassString
{
    /// <summary>
    /// Этот метод обработки строки.
    /// </summary>
    public class StringWork
    {
        string text;
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="text">Строка настройка.</param>
        public StringWork(string text)
        {
            this.text = text;
        }
        /// <summary>
        /// Конструктор.
        /// </summary>
        public StringWork()
        {
        }
        /// <summary>
        /// Способ вырезания повторяющихся слов.
        /// </summary>
        /// <returns>Строка без повторяющихся слов.</returns>
        public string SplitProposal()
        {
            string data = "";
            string[] separator = { ". ", "! ", ".", "!" };
            string[] sentences = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            string[] words;
            for (int i = 0; i < sentences.Length; i++)
            {
                words = sentences[i].Split(' ');
                for (int j = 0; j < words.Length - 1; j++)
                {
                    if (words[j].ToLower() != words[j + 1].ToLower())
                    {
                        data += words[j] + " ";
                    }
                }
                data += words[words.Length - 1] + ". ";
            }
            return data;
        }
        /// <summary>
        /// Этот метод разбивает текст на предложения и проверяет их.
        /// </summary>
        /// <returns>Если строка верна-true, иначе-false.</returns>
        public bool Checking()
        {
            Regex site = new Regex(@"http:\/\/\.\S+\.\S{3}|\S{2}");
            Regex data = new Regex(@"(([0-2][0-9])|(3[0-1]))\/((0[0-9])|(1[0-2]))\/\d{2}");
            Regex time = new Regex(@"(([0-1][0-9])|(2[0-3]))-[0-5][0-9]");
            string[] separator = { ". " };
            string[] sentences = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            int counter = 0;
            for (int i = 0; i < sentences.Length; i++)
            {
                if ((site.IsMatch(sentences[i])) && (data.IsMatch(sentences[i])) && (time.IsMatch(sentences[i])))
                {
                    counter++;
                }
            }
            if (counter == sentences.Length)
                return true;
            else return false;
        }
        /// <summary>
        /// метод form StringBuilder.
        /// </summary>
        /// <returns>Строка.</returns>
        public StringBuilder FormString()
        {
            if (Checking())
            {
                Regex data = new Regex(@"(([0-2][0-9])|(3[0-1]))\/((0[0-9])|(1[0-2]))\/\d{2}");
                string[] separator = { ". " };
                string[] sentences = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                string newString = "";
                for (int i = 0; i < sentences.Length; i++)
                {
                    MatchCollection dates = data.Matches(sentences[i]);
                    if (dates.Count == 1)
                    {
                        newString += sentences[i].Substring(0, sentences[i].Length - 6) + "\n";
                    }
                }
                StringBuilder information = new StringBuilder(newString);
                return information;
            }
            else
            {
                return null;
            }
        }
    }
}