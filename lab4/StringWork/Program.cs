﻿using System;
using ClassString;
using System.Text;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu = 0;
            string data = "";
            StringWork sentence = new StringWork();
            do
            {
                Console.WriteLine(" ");
                Console.WriteLine("*************МЕНЮ*************");
                Console.WriteLine("1. Удалить повторяющиеся слова");
                Console.WriteLine("2. Имя сайта, дату и время входа в систему");
                Console.WriteLine("3. Выход");
                Console.WriteLine("******************************");
                Console.Write("Выберите пункт меню: ");
                Int32.TryParse(Console.ReadLine(), out menu);
                switch (menu)
                {
                    case 1:
                        Console.WriteLine("Введите предложение с повторяющимися словами:");
                        // зима зима пришла пришла пришла неожиданно для жителей деревни.
                        data = Console.ReadLine();
                        sentence = new StringWork(data);
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine(sentence.SplitProposal());
                        break;
                    case 2:
                        Console.WriteLine("Введите имя сайта, дату и время входа в систему.");
                        Console.WriteLine("Например, http://ya.ru 31/01/19 18-30.");
                        //http://ya.ru 12/01/19 19-00. http://rt.te 15/02/19 12-30 16/03/12 19-40. http://ght.re 18/11/15 12-30.
                        //string data = "http://ya.ru 12/01/19 18-30 12/01/19 18-30. http://yaпке.ru 31/01/19 18-30. http://yajпа.ru 31/01/19 18-30.";
                        data = Console.ReadLine();
                        sentence = new StringWork(data);
                        if (!sentence.Checking())
                        {
                            Console.WriteLine("Ошибка:неверные данные ");
                        }
                        
                            //tre.tr 32/12/18 19-30.
                            //http://ref.df 31/01/19 18-30.
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine(sentence.FormString());
                        break;
                }
            } while (menu != 3);
            Console.ReadKey();
        }
    }
}