﻿using System;
using ClassParser;

namespace StringParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Phone[] numbers = null;
            int count;
            //string[][] formString;
            Parser phoneNumbers = null;


            Console.WriteLine("Сколько телефонных номеров вы хотите ввести?");
            Int32.TryParse(Console.ReadLine(), out count);
            numbers = new Phone[count];

            //+375447252889
            //45896
            //447254589
            //7254289
            //5813021
            //+375447524289
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Введите номер телефона : ");
                string str = Console.ReadLine();

                phoneNumbers = new Parser(str);
                if (phoneNumbers.Checking())
                {
                    Console.WriteLine("Строка введена правильно.");
                    Console.WriteLine("\n");
                    numbers[i] = new Phone(str);
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Строка введена неверно. Повторите запись!");
                    Console.WriteLine("\n");
                    i--;
                }
            }

            
           foreach (Phone item in numbers)
            {
                Console.WriteLine(item.Output());
            }

            Console.ReadKey();

            //if (phoneNumbers.Checking())
            //{
            //    Console.WriteLine("Строка введена правильно.");
            //    Console.WriteLine("\n");

            //    formString = phoneNumbers.Form();

            //    Console.WriteLine(phoneNumbers.Output(formString));
            //    Console.ReadKey();
            //}
            //else
            //{
            //    Console.WriteLine("Строка введена неверно. Повторите запись!");
            //    Console.WriteLine("\n");
            //    Console.WriteLine("Сколько телефонных номеров вы хотите ввести?");
            //    Int32.TryParse(Console.ReadLine(), out count);
            //    numbers = new string[count];
            //    Console.WriteLine("Введите номер телефона : ");
            //    for (int i = 0; i < count - 1; i++)
            //    {
            //        numbers[i] = Console.ReadLine();
            //    }
            //    phoneNumbers = new Parser(numbers);
            //}
        }
    }
}