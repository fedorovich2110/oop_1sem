﻿using System;
using System.Text.RegularExpressions;

namespace ClassParser
{
    /// <summary>
    /// Этот класс обрабатывает строки.
    /// </summary>

    public class Parser
    {
        string phone;
        string[] phoneNumbers;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="phoneNumbers">строковый массив.</param>
        public Parser(string phone)
        {            
            this.phone = phone;
            method(phone);
        }

        private void method(string phone)
        {
            phoneNumbers = new string[phone.Length];
            char[] p = phone.ToCharArray();
            for (int i = 0; i < p.Length; i++)
            {
                phoneNumbers[i] = p[i].ToString();
            }
        }

        /// <summary>
        /// Этот метод проверяет введенную строку.
        /// </summary>
        /// <returns>True если строка является числом и false если нет.</returns>
        public bool Checking()
        {
            //try
            //{
            //    string i = (phone.Remove(0, 1));
            //    if (Convert.ToInt32(i) > 0) 
            //    {
            //        return true;
            //    }
            //    else if (Convert.ToInt32(phone) > 0)
            //    {
            //        return true;
            //    }
            //    else return false;
            //}
            //catch
            //{
            //    return false;
            //}

            int count = 0;
            for (int i = 0; i < phoneNumbers.Length; i++)
            {
                if (phoneNumbers[i].Equals("+"))
                {
                    count++;
                }

                if (Int64.TryParse(phoneNumbers[i], out long result))
                {
                    //Console.WriteLine(result);
                    if (result >= 0 )
                        count++;
                }
            }

            if (count == (phoneNumbers.Length))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Этот метод сравнивает строку с регулярным выражением и записывает ее в новый массив, если она совпадает.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        //private string[] CreateArray(Regex number)
        //{
        //    string[] array = new string[phoneNumbers.Length];
        //    int count = 0;
        //    for (int i = 0; i < phoneNumbers.Length; i++)
        //    {
        //        if ((number.IsMatch(phoneNumbers[i])))
        //        {
        //            array[count] = phoneNumbers[i];
        //            count++;
        //        }
        //    }
        //    return array;
        //}

        /// <summary>
        /// В этом методе мы записываем проверенные значения чисел в массив.
        /// </summary>
        /// <returns>Массив.</returns>
        //public TypeOfNumber /*string[][]*/ Form()
        //{

        //Regex fullNumber = new Regex(@"\A\+((\d{3})|(\d{2})|(\d{1}))\d{2}\d{7}");
        //Regex codeNumber = new Regex(@"\A\d{9}");
        //Regex number = new Regex(@"\A\d{7}\Z");
        //string[][] answer = new string[3][];
        //answer[0] = CreateArray(fullNumber);
        //answer[1] = CreateArray(codeNumber);
        //answer[2] = CreateArray(number);
        //return answer;

        //Checking();
        //TypeOfNumber type = new TypeOfNumber();
        //  for (int i = 0; i < phone.Length; i++)
        //{
        //  if (fullNumber.IsMatch(phone))
        //{
        //type = TypeOfNumber.full;
        //}
        //          else if (codeNumber.IsMatch(phone))
        //        {
        //type = TypeOfNumber.withCode;
        //}
        //          else if (number.IsMatch(phone))
        //        {
        //type = TypeOfNumber.withoutCode;
        //} 
        //          else type = TypeOfNumber.lose;
        //}
        //      return type;
        //}



    }
}