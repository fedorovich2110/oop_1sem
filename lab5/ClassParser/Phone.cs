﻿using System;
using System.Text.RegularExpressions;

namespace ClassParser
{
    // перечисление 
    public enum TypeOfNumber
    {
        full,
        withCode,
        withoutCode,
        lose
    }

    public class Phone
    {
        /// <summary>
        /// Свойство тип номера
        /// </summary>
        public TypeOfNumber TypeNumber { get; set; }

        /// <summary>
        ///  номер.
        /// </summary>
        public string Number { get; set; }



        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="number">номер.</param>
        public Phone(string number)
        {
                Number = number;
                TypeNumber = GerTypeNumber();      
        }

        /// <summary>
        /// Вывод номеров.
        /// </summary>
        public string Output()
        {
            string answer = "";
            if (TypeNumber is TypeOfNumber.full)
            {
                answer = "Полный номер: \n"+ Number;
            }
            else if (TypeNumber is TypeOfNumber.withCode)
            {
                answer += "Номер с кодом: \n" + Number;
            }
            else if (TypeNumber is TypeOfNumber.withoutCode)
            {
                answer += "Номер без кода: \n" + Number;
            }
            //else answer = "Ошибка! это не мобильный номер";

            return answer;
        }

        private TypeOfNumber GerTypeNumber()
        {

            Regex fullNumber = new Regex(@"\A\+((\d{3})|(\d{2})|(\d{1}))\d{2}\d{7}");
            Regex codeNumber = new Regex(@"\A\d{9}");
            Regex number = new Regex(@"\A\d{7}\Z");

            TypeOfNumber type = new TypeOfNumber();
            for (int i = 0; i < Number.Length; i++)
            {
                if (fullNumber.IsMatch(Number))
                {
                    type = TypeOfNumber.full;
                }
                else if (codeNumber.IsMatch(Number))
                {
                    type = TypeOfNumber.withCode;
                }
                else if (number.IsMatch(Number))
                {
                    type = TypeOfNumber.withoutCode;
                }
                else type = TypeOfNumber.lose;
            }
            return type;
        }


        /// <summary>
        /// Этот метод проверяет введенную строку.
        /// </summary>
        /// <returns>True если строка является числом и false если нет.</returns>
        public bool Checking()
        {
            try
            {
                if (Convert.ToInt32(Number) > 0)
                {
                    return true;
                }
                else if (Convert.ToInt32((Number.Remove(0, 1))) > 0)
                {
                    return true;
                }
                else return false;
            }
            catch
            {
                return false;
            }
        }

    }
}