﻿using System;
using ClassArray;

namespace ArrayWork
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu;
            int lengthA = default;
            int lengthB = default;
            int lengthC = default;
            Array3 A = default;
            Array3 B = default;
            Array3 C = default;
            int[] arrayA = default;
            int[] arrayB = default;
            int[] arrayC = default;

            Console.Write("Введите длину массива A: ");
            Int32.TryParse(Console.ReadLine(), out lengthA);
            A = new Array3(lengthA);
            arrayA = new int[lengthA];
            for (int i = 0; i < lengthA; i++)
            {
                Int32.TryParse(Console.ReadLine(), out arrayA[i]);
            }
            A.InputArray(arrayA, lengthA);
            Console.Write("Введите длину массива B: ");
            Int32.TryParse(Console.ReadLine(), out lengthB);
            B = new Array3(lengthB);
            arrayB = new int[lengthB];
            for (int i = 0; i < lengthB; i++)
            {
                Int32.TryParse(Console.ReadLine(), out arrayB[i]);
            }
            B.InputArray(arrayB, lengthB);
            Console.Write("Введите длину массива C: ");
            Int32.TryParse(Console.ReadLine(), out lengthC);
            C = new Array3(lengthC);
            arrayC = new int[lengthC];
            for (int i = 0; i < lengthC; i++)
            {
                Int32.TryParse(Console.ReadLine(), out arrayC[i]);
            }
            C.InputArray(arrayC, lengthC);
            
            do
            {
                Console.WriteLine(" ");
                Console.WriteLine("*************Меню*************");
                Console.WriteLine("1. Введите массивы");
                Console.WriteLine("2. Выходные массивы");
                Console.WriteLine("3. Проверка");
                Console.WriteLine("4. Общее количество положительных элементов в массиве 5+A и C+2");
                Console.WriteLine("5. Общее количество положительных элементов в массиве 2+B, A+5 и C+4");
                Console.WriteLine("6. Если B[] имеют нули");
                Console.WriteLine("7. Вывод");
                Console.WriteLine("******************************");
                Console.Write("Выберите пункт меню: ");
                Int32.TryParse(Console.ReadLine(), out menu);
                switch (menu)
                {
                    case 2:
                        Console.WriteLine("Массив A:");
                        Console.WriteLine(A.ToSring()); //преобразователь строки
                        Console.WriteLine("Массив B:");
                        Console.WriteLine(B.ToSring());
                        Console.WriteLine("Массив C:");
                        Console.WriteLine(C.ToSring());
                        break;
                    case 3:
                        if (A.Checking())
                        {
                            Console.WriteLine("В массиве A нет нулей .");
                        }
                        else
                        {
                            Console.WriteLine("В массиве A есть нули .");
                        }
                        if (B.Checking())
                        {
                            Console.WriteLine("В массиве B нет нулей .");
                        }
                        else
                        {
                            Console.WriteLine("В массиве B есть нули .");
                        }
                        if (C.Checking())
                        {
                            Console.WriteLine("В массиве C нет нулей .");
                        }
                        else
                        {
                            Console.WriteLine("В массиве C есть нули .");
                        }
                            break;
                    case 4:
                        int quantity;
                        quantity = Array3.PositiveNumber(A.IncreaseIntNumber(5), C.IncreaseIntNumber(2));
                        Console.WriteLine("Массив A:");
                        Console.WriteLine(A.ToSring());
                        Console.WriteLine("Массив C:");
                        Console.WriteLine(C.ToSring());
                        Console.WriteLine("Общее количество положительных элементов:");
                        Console.WriteLine(quantity);
                        break;
                    case 5:
                        int quant;
                        quant = Array3.PositiveNumber(arrayA, B.IncreaseIntNumber(2), C.IncreaseIntNumber(4));
                        Console.WriteLine("Массив A:");
                        Console.WriteLine(A.ToSring());
                        Console.WriteLine("Массив B:");
                        Console.WriteLine(B.ToSring());
                        Console.WriteLine("Массив C:");
                        Console.WriteLine(C.ToSring());
                        Console.WriteLine("Общее количество положительных элементов:");
                        Console.WriteLine(quant);
                        break;
                    case 6:
                        int averageSum = 0;
                        if ((A.Checking()) && (Array3.PositiveNumber(arrayA) > 3) && !(B.Checking()))
                        {
                            for (int i = 0; i < lengthA; i++)
                            {
                                averageSum += A[i];
                            }
                            averageSum /= lengthA;
                            for (int i = 0; i < lengthB; i++)
                            {
                                B[i] = averageSum;
                            }
                            Console.WriteLine(B.ToSring());
                        }
                        else
                        {
                            Console.WriteLine("Нет нулей");
                        }
                        break;
                }
            } while (menu != 7);
        }
    }
}