﻿using System;

namespace ClassArray
{
    /// <summary>
    /// Класс Array3.
    /// Содержит методы для работы с массивами.
    /// </summary>
    public class Array3
    {
        /// <summary>
        /// Это свойство ArrayLength.
        /// </summary>
        public int ArrayLength
        {
            get
            {
                return array.Length;
            }
        }

        private int[] array;
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="length">Переменная int. Длина массива.</param>
        public Array3(int length) //Конструктор с параметрами, 
        {
            array = new int[length];
        }
        /// <summary>
        /// индексы.
        /// </summary>
        /// <param name="index">>Переменная типа int.</param>
        /// <returns> Array with index.</returns>
        public int this[int index] //Индексатор для доступа к элементам поля-массива
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }
        /// <summary>
        /// Этот метод вводит массив.
        /// </summary>
        /// <param name="Array"> int Массив.</param>
        /// <param name="ArrayLength">Интервал изменчивым. Длина массива.</param>
        public void InputArray(int[] Array, int ArrayLength) //Методы вывода и ввода массива
        {
            for (int i = 0; i < ArrayLength; i++)
            {
                this.array[i] = Array[i];
            }
        }
        /// <summary>
        /// Этот метод выводит массив.
        /// </summary>
        /// <returns>Строковая переменная.</returns>
        public string ToSring()
        {
            string answer = "";
            for (int i = 0; i < ArrayLength; i++)
            {
                answer += i+1 + ". " + array[i] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Этот метод проверяет массив на наличие нулей.
        /// </summary>
        /// <returns>Переменная bool. True, если массив не имеет нуля, и false, если массив имеет ноль.</returns>

        public bool Checking()
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        ///Этот метод добавляет число int в массив.
        /// </summary>
        /// <param name="k"> Переменная int.</param>
        /// <returns>Массив с новыми значениями.</returns>

        public int[] IncreaseIntNumber(int k)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += k;
            }
            return array;
        }
        /// <summary>
        /// Этот метод проверяет наличие положительных значений в массиве и добавляет их количество.
        /// </summary>
        /// <param name="data">Массив int.</param>
        /// <returns>Количество положительных чисел.</returns>

        public static int PositiveNumber(params Array[] data)
        {
            int kolvo = 0;
            for (int i = 0; i < data.Length; i++)
            {
                int[] Array = (int[])data[i];
                for (int j = 0; j < Array.Length; j++)
                {
                    if (Array[j] > 0)
                    {
                        kolvo++;
                    }
                }
            }
                
            return kolvo;
        }
    }
}