﻿using System;
using ClassArray;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestArray
{
    /// <summary>
    /// Класс, который тестирует программу.
    /// </summary>
    [TestClass]
    public class UnitTestArray
    {
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод plus со значениями 5.
        /// </summary>
        [TestMethod]
        public void Plus_5()
        {
            // Организовать
            int[] array = { 1, 2, 3 };
            Array3 x = new Array3(3);
            x.InputArray(array, 3);
            int[] expected = { 6, 7, 8 }; //ожидаемый ответ
            // Действие
            int[] actual = x.IncreaseIntNumber(5);
            // Утверждать
            CollectionAssert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Этот тест проверяет, правильно ли работает метод plus со значениями -10.
        /// </summary>

        [TestMethod]

        public void Plus_negative10()
        {
            // Организовать
            int[] array = { 0, -2, 8 };
            Array3 x = new Array3(3);
            x.InputArray(array, 3);
            int[] expected = { -10, -12, -2 };
            // Действие
            int[] actual = x.IncreaseIntNumber(-10);
            // Утверждать
            CollectionAssert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод plus со значениями -1.
        /// </summary>

        [TestMethod]

        public void Plus_negative1()
        {
            // Организовать
            int[] array = { 0, -2, 8, 18, -20 };
            Array3 x = new Array3(5);
            x.InputArray(array, 5);
            int[] expected = { 1, -1, 9, 19, -19 };
            // Действие
            int[] actual = x.IncreaseIntNumber(1);
            // Утверждать
            CollectionAssert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Этот тестовый метод проверки нулей корректно работает со значениями 0, 1, 2 в массиве.
        /// </summary>

        [TestMethod]

        public void Checking1()
        {
            // Организовать
            int[] array = { 0, 1, 2 };
            Array3 x = new Array3(3);
            x.InputArray(array, 3);
            bool expected = false;
            //действие
            bool actual = x.Checking();
            //утверждать
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Этот тестовый метод проверки нулей корректно работает со значениями 0, -2, 8 в массиве.
        /// </summary>

        [TestMethod]

        public void Checking2()
        {
            // организовать
            int[] array = { 1, -2, 8 };
            Array3 x = new Array3(3);
            x.InputArray(array, 3);
            bool expected = true;
            //действие
            bool actual = x.Checking();
            // утверждать
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тестовый метод проверки нулей корректно работает со значениями 0, 1, 2 в массиве.
        /// </summary>

        [TestMethod]

        public void Checking3()
        {
            // организовать
            int[] array = { 1, -2, 8, 0, 0 };
            Array3 x = new Array3(5);
            x.InputArray(array, 5);
            bool expected = false;
            //действие
            bool actual = x.Checking();
            // утверждать
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод положительных чисел со значениями 0, -2, 8 в массиве A и 3, -8, 7 в массиве arrayB.
        /// </summary>

        [TestMethod]

        public void PositiveNumber()
        {
            // организовать
            int[] arrayA = { 0, -2, 8 };
            int[] arrayB = { 3, -8, 7 };
            Array3 x1 = new Array3(3);
            Array3 x2 = new Array3(3);
            x1.InputArray(arrayA, 3);
            x2.InputArray(arrayB, 3);
            int expected = 3;
            // действие
            int actual = Array3.PositiveNumber(arrayA) + Array3.PositiveNumber(arrayB);
            // утверждать
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод положительных чисел со значениями 0, -2, 8 в массиве A и -3, -8, 0 в массиве B.
        /// </summary>

        [TestMethod]

        public void PositiveNumber2()
        {
            int[] arrayA = { 0, -2, -8 };
            int[] arrayB = { -3, -8, 0 };
            Array3 x1 = new Array3(3);
            Array3 x2 = new Array3(3);
            x1.InputArray(arrayA, 3);
            x2.InputArray(arrayB, 3);
            int expected = 0;
            int actual = Array3.PositiveNumber(arrayA) + Array3.PositiveNumber(arrayB);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод положительных чисел со значениями 1, 2, 8 в массиве A и 3, 8, 9, 8, 5, 6 в arrayB.
        /// </summary>

        [TestMethod]

        public void PositiveNumber3()
        {
            // организовать
            int[] arrayA = { 1, 2, 8 };
            int[] arrayB = { 3, 8, 9, 8, 5 };
            Array3 x1 = new Array3(3);
            Array3 x2 = new Array3(5);
            x1.InputArray(arrayA, 3);
            x2.InputArray(arrayB, 5);
            int expected = 8;
            // действие
            int actual = Array3.PositiveNumber(arrayA) + Array3.PositiveNumber(arrayB);
            // утверждать
            Assert.AreEqual(expected, actual);
        }
    }
}