﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassStudentList;

namespace UnitTestStudent
{
    /// <summary>
    /// Класс, который проверяет программу.
    /// </summary>
    [TestClass]

    public class UnitTest1
    {
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод средней отметки со значениями 10 и 6.
        /// </summary>
        [TestMethod]

        public void AvarageMark_10and6_return8()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 10);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 6);
            float expected = 8;
            float actual = Student.AverageMark(x);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод средней отметки со значениями 0 и 10.
        /// </summary>
        [TestMethod]

        public void AvarageMark_2and10_return6()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 2);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 10);
            float expected = 6;
            float actual = Student.AverageMark(x);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод средней отметки со значением 5, 7, 8, 4.
        /// </summary>
        [TestMethod]
        public void AvarageMark_5and7and8and4_return7()
        {
            Student[] x = new Student[4];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 5);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 7);
            x[2] = new Student("Kniga", "Ruslan", "Pavlovich", 8);
            x[3] = new Student("Ivanova", "Liza", "Sergeevna", 4);
            float expected = 6;
            float actual = Student.AverageMark(x);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод средней отметки со значением 1, 0, 1 and 1.
        /// </summary>
        [TestMethod]

        public void AvarageMark_1and0and1and1_return7()
        {
            // Arrange
            Student[] x = new Student[4];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 1);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 0);
            x[2] = new Student("Kniga", "Ruslan", "Pavlovich", 1);
            x[3] = new Student("Ivanova", "Liza", "Sergeevna", 1);
            double expected = 0.75;
            float actual = Student.AverageMark(x);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод средней отметки со значением 1 and 0.
        /// </summary>
        [TestMethod]

        public void AvarageMark_1and0_return05()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 1);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 0);
            double expected = 0.5;
            float actual = Student.AverageMark(x);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Этот тест проверяет, правильно ли работает метод перегрузки оператора plus со строками двух объектов.
        /// </summary>
        [TestMethod]

        public void Plus_x1andx2_return_FedorovichSidorov()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 9);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 8);
            string expected = "FedorovichSidorov";
            string actual = x[0].OutputLastName() + x[1].OutputLastName();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора plus со значениями 3 и 0.
        /// </summary>
        [TestMethod]

        public void Plus_3and0_return3()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 3);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 0);
            float expected = 3;
            float actual = x[0].OutputGrade() + x[1].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Этот тест проверяет, правильно ли работает метод перегрузки оператора plus со строками из четырех объектов.
        /// </summary>
        [TestMethod]

        public void PLus_x0andx1andx2andx3_returnFedorovichSidorovKnigaIvanovaa()
        {
            Student[] x = new Student[4];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 1);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 0);
            x[2] = new Student("Kniga", "Ruslan", "Pavlovich", 1);
            x[3] = new Student("Ivanova", "Liza", "Sergeevna", 1);
            string expected = "FedorovichSidorovKnigaIvanova";
            string actual = x[0].OutputLastName() + x[1].OutputLastName() + x[2].OutputLastName() + x[3].OutputLastName();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора plus со значением 3, 8 и 7.
        /// </summary>
        [TestMethod]

        public void Plus_3and8and7_return18()
        {
            Student[] x = new Student[3];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 3);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 8);
            x[2] = new Student("Vasnecov", "Oleg", "Evgenevich", 7);
            float expected = 18;
            float actual = x[0].OutputGrade() + x[1].OutputGrade() + x[2].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора plus со значением 10, 0 и 0.
        /// </summary>
        [TestMethod]

        public void Plus_10and0and0_return18()
        {
            Student[] x = new Student[3];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 10);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 0);
            x[2] = new Student("Vasnecov", "Oleg", "Evgenevich", 0);
            float expected = 10;
            float actual = x[0].OutputGrade() + x[1].OutputGrade() + x[2].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора минус со значением 8 и 6.
        /// </summary>
        [TestMethod]

        public void Minus_8and6_return3()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 8);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 6);
            float expected = 2;
            float actual = x[0].OutputGrade() - x[1].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора минус со значениями 0 и 5.
        /// </summary>
        [TestMethod]

        public void Minus_0and5_return3()
        {
            Student[] x = new Student[2];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 0);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 5);
            float expected = -5;
            float actual = x[0].OutputGrade() - x[1].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора минус со значением 10, 5 и 2.
        /// </summary>
        [TestMethod]

        public void Minus_10and5and2_return18()
        {
            Student[] x = new Student[3];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 10);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 5);
            x[2] = new Student("Vasnecov", "Oleg", "Evgenevich", 2);
            float expected = 3;
            float actual = x[0].OutputGrade() - x[1].OutputGrade() - x[2].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора минус со значением 0, 3 и 2.
        /// </summary>

        [TestMethod]

        public void Minus_0and3and2_return5()
        {
            Student[] x = new Student[3];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 0);
            x[1] = new Student("Sidorov", "Andrew", "Sergeevich", 3);
            x[2] = new Student("Vasnecov", "Oleg", "Evgenevich", 2);
            float expected = -5;
            float actual = x[0].OutputGrade() - x[1].OutputGrade() - x[2].OutputGrade();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Этот тест проверяет, правильно ли работает метод перегрузки оператора минус со строками двух объектов.
        /// </summary>

        [TestMethod]

        public void Minus_x0andx1_returnFedorovichSidorov()
        {
            Student[] x = new Student[4];
            x[0] = new Student("Fedorovich", "Anastasia", "Dmitrievna", 10);
            x[1] = new Student("Sidorov", "Artem", "Semenovich", 2);
            string expected = "FedorovichSidorov";
            // Метод вычисления
            string actual = x[0].OutputLastName() + x[1].OutputLastName();
            // Сравнение
            Assert.AreEqual(expected, actual);
        }
    }
}