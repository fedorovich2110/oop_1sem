﻿using System;
using ClassStudentList;

namespace StudentProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu;
            string lastName;
            string name;
            string patronymic;
            float examGrade;
            int n;

            Console.Write("Введите количество студентов: ");
            Int32.TryParse(Console.ReadLine(), out n);

            while (!Student.CheckNumbers(n))
            {
                Console.WriteLine("Неккоректный ввод.");
                Console.Write("Введите количество студентов: ");
                Int32.TryParse(Console.ReadLine(), out n);
            }

            Student[] student = new Student[n];


            for (int i = 0; i < n; i++)
            {
                do
                {
                    Console.Write("Введите Фамилию: ");
                    lastName = Console.ReadLine();
                } while (!Student.CheckNullString(lastName));

                do
                {
                    Console.Write("Введите имя: ");
                    name = Console.ReadLine();

                } while (!Student.CheckNullString(name));

                do
                {
                    Console.Write("Введите Отчество: ");
                    patronymic = Console.ReadLine();

                } while (!Student.CheckNullString(patronymic));

                Console.Write("Введите оценку экзамена: ");
                float.TryParse(Console.ReadLine(), out examGrade);

                while (!Student.CheckMarks(examGrade))
                {
                    Console.WriteLine("Некоректный ввод.");
                    Console.Write("Введите оценку экзамена: ");
                    float.TryParse(Console.ReadLine(), out examGrade);
                }

                student[i] = new Student(lastName, name, patronymic, examGrade);

            }

            do
            {
                Console.WriteLine(" ");
                Console.WriteLine("*************MENU*************");
                Console.WriteLine("1. Список студентов");
                Console.WriteLine("2. Сложение");
                Console.WriteLine("3. Вычитание");
                Console.WriteLine("4. Средний балл студентов");
                Console.WriteLine("5. Выход");
                Console.WriteLine("******************************");
                Console.Write("Выберите пункт меню: ");

                Int32.TryParse(Console.ReadLine(), out menu);

                switch (menu)
                {
                    case 1: //выводит всех студентов

                        for (int i = 0; i < n; i++)
                        {

                            Console.WriteLine((i + 1) + ". " + student[i].OutputList());

                        }

                        break;

                    case 2: //сумма

                        int m;
                        int j;

                        Student sum;
                        Console.Write("Введите номер первого студента: ");
                        Int32.TryParse(Console.ReadLine(), out m);

                        Console.Write("Введите номер второго студента: ");
                        Int32.TryParse(Console.ReadLine(), out j);
                        if(j >= student.Length)
                        {
                            Console.WriteLine("Ошибка");
                            break;
                        }
                        sum = student[m - 1] + student[j - 1];

                        Console.WriteLine(sum.Output());

                        break;

                    case 3: //разность
                        Student difference;

                        Console.Write("Введите номер первого студента: ");
                        Int32.TryParse(Console.ReadLine(), out m);

                        Console.Write("Введите номер второго студента: ");
                        Int32.TryParse(Console.ReadLine(), out j);

                        difference = student[m - 1] - student[j - 1];

                        Console.WriteLine(difference.Output());

                        break;

                    case 4: //средний балл

                        Console.Write("Средняя оценка студентов: ");
                        Console.WriteLine(Student.AverageMark(student));

                        break;
                }
            } while (menu != 5);
        }
    }
}