﻿using System;

namespace ClassStudentList
{
    /// <summary>
    /// Студенческий класс.
    /// Содержит методы для работы с типом учащегося.
    /// </summary>

    public class Student
    {
        string LastName; //поля
        string Name;
        string Patronymic;
        float ExamGrade;
        /// <summary>
        /// Метод, который создает объект класса - конструктор.
        /// </summary>
        /// <param name="LastName">Строковая переменная. Фамилия студента.</param>
        /// <param name="Name">Строковая переменная. Имя студента.</param>
        /// <param name="Patronymic">Строковая переменная. Отчество студента.</param>
        /// <param name="ExamGrade">Переменная с плавающей запятой. Экзаменационная оценка студента.</param>

        public Student(string LastName, string Name, string Patronymic, float ExamGrade) //конструктор
        {
            this.LastName = LastName;
            this.Name = Name;
            this.Patronymic = Patronymic;
            this.ExamGrade = ExamGrade;
        }

        public Student()
        {
        }

        /// <summary>
        /// Этот метод проверяет, является ли число отрицательным.
        /// </summary>
        /// <param name="ExamGrade"></param>
        /// <returns>Ложь если отрицательно и истина если положительно.</returns>

        static public bool CheckNumbers(float ExamGrade)
        {
            if (ExamGrade <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Этот метод проверяет, является ли число отрицательным и меньше 10.
        /// </summary>
        /// <param name="ExamGrade"></param>
        /// <returns>Ложь, если отрицательный и истинный, если положительный и менее 10.</returns>

        static public bool CheckMarks(float ExamGrade)
        {
            if ((ExamGrade >= 0) && (ExamGrade <= 10))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        ///Этот метод проверяет, является ли строка null или пробелом.
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>Значение false, если строка является нулем или белое пространство, и true, если заполнены.</returns>

        static public bool CheckNullString(string Name)
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Этот метод создает строку для перечисления студентов.
        /// </summary>
        /// <returns>строка из списка.</returns>

        public string OutputList()
        {
            return LastName + " " + Name + " " + Patronymic + " Mark: " + ExamGrade;
        }

        /// <summary>
        /// Метод перегрузки оператора плюс.
        /// </summary>
        /// <param name="c1">The object of Student class.</param>
        /// <param name="c2">The object of Student class.</param>
        /// <returns>Новый объект.</returns>

        public static Student operator +(Student c1, Student c2)
        {
            // Создайте новый объект класса Student.
            Student student = new Student();
            // Проверьте, совпадают ли имена, фамилии и отчества.
            if (!String.Equals(c1.LastName, c2.LastName) || !String.Equals(c1.Name, c2.Name) || !String.Equals(c1.Patronymic, c2.Patronymic))
            {
                student.LastName = c1.LastName + " " + c2.LastName + "\n";
                student.Name = c1.Name + " " + c2.Name + "\n";
                student.Patronymic = c1.Patronymic + " " + c2.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade + c2.ExamGrade;
                return student;
            }
            else
            {
                student.LastName = c1.LastName + "\n";
                student.Name = c1.Name + "\n";
                student.Patronymic = c1.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade;
                return student;
            }
        }

        /// <summary>
        /// Метод вывода строки для оператора плюс и минус в основной программе.
        /// </summary>
        /// <returns>Строка с именем, фамилией, отчеством и пометкой.</returns>

        public string Output()
        {
            return LastName + Name + Patronymic + "Mark: " + ExamGrade;
        }

        /// <summary>
        /// Метод перегрузки оператора минус.
        /// </summary>
        /// <param name="c1">The object of Student class</param>
        /// <param name="c2">The object of Student class</param>
        /// <returns>New object.</returns>

        public static Student operator -(Student c1, Student c2)
        {
            Student student = new Student();
            //Проверьте, совпадают ли имена, фамилии и отчества..
            if (!String.Equals(c1.LastName, c2.LastName) || !String.Equals(c1.Name, c2.Name) || !String.Equals(c1.Patronymic, c2.Patronymic))
            {
                student.LastName = c1.LastName + " " + c2.LastName + "\n";
                student.Name = c1.Name + " " + c2.Name + "\n";
                student.Patronymic = c1.Patronymic + " " + c2.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade - c2.ExamGrade;
                return student;
            }
            else
            {
                student.LastName = c1.LastName + "\n";
                student.Name = c1.Name + "\n";
                student.Patronymic = c1.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade;
                return student;
            }
        }

        /// <summary>
        /// Метод для использования экзаменационной оценки в модульных тестах.
        /// </summary>
        /// <returns>Номер поплавка.</returns>

        public float OutputGrade()
        {
            return ExamGrade;
        }

        /// <summary>
        /// Метод для использования фамилии в модульном тесте.
        /// </summary>
        /// <returns>Метод строка фамилия.</returns>

        public string OutputLastName()
        {
            return LastName;
        }

        /// <summary>
        /// Метод, который учитывает среднюю оценку всех студентов.
        /// </summary>
        /// <param name="c1">Массив объектов класса student.</param>
        /// <returns>The float number average mark.</returns>

        static public float AverageMark(Student[] c1)
        {
            float Sum = 0;
            // Цикл суммировал все отметки.
            for (int i = 0; i < c1.Length; i++)
            {
                Sum += c1[i].ExamGrade;
            }
            Sum = Sum / c1.Length;
            return Sum;
        }
    }
}