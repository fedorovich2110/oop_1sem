﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentInfoLibrary;
using ExceptionLibrary;

namespace StudentInfoUnitTest
{
    [TestClass]
    public class StudentTest
    {
        /// <summary>
        /// This text checking exception on empty line in method OutputList.
        /// </summary>
        [TestMethod]
        public void OutputList1()
        {
            //Arrange
            Student stud = new Student("", "", "", 0);
            //Assert
            Assert.ThrowsException<EmptyLineException>(() => stud.OutputList());
        }
        /// <summary>
        /// This text checking exception in method OutputList.
        /// </summary>
        [TestMethod]
        public void OutputList2()
        {
            //Arrange
            Student stud = new Student("Иванов", "", "", 10);
            //Assert
            Assert.ThrowsException<EmptyLineException>(() => stud.OutputList());
        }
        /// <summary>
        /// This test checks if the avarage mark method works correctly with values of 9 and 5.
        /// </summary>
        [TestMethod]
        public void AvarageMark_9and5_return7()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 9);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 5);
            float expected = 7;
            // Act
            float actual = Student.AverageMark(x, 2);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the avarage mark method works correctly with values of 0 and 10.
        /// </summary>
        [TestMethod]
        public void AvarageMark_0and10_return5()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 0);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 10);
            float expected = 5;
            // Act
            float actual = Student.AverageMark(x, 2);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the avarage mark method works correctly with value of 5, 7, 8, 4.
        /// </summary>
        [TestMethod]
        public void AvarageMark_5and7and8and4_return7()
        {
            // Arrange
            Student[] x = new Student[4];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 5);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 7);
            x[2] = new Student("Alekseev", "Misha", "Pavlovich", 8);
            x[3] = new Student("Semenova", "Alina", "Sergeevna", 4);
            float expected = 6;
            // Act
            float actual = Student.AverageMark(x, 4);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the avarage mark method works correctly with values of 1, 0, 1 and 1.
        /// </summary>
        [TestMethod]
        public void AvarageMark_1and0and1and1_return7()
        {
            // Arrange
            Student[] x = new Student[4];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 1);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 0);
            x[2] = new Student("Alekseev", "Misha", "Pavlovich", 1);
            x[3] = new Student("Semenova", "Alina", "Sergeevna", 1);
            double expected = 0.75;
            // Act
            float actual = Student.AverageMark(x, 4);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the avarage mark method works correctly with values of 1 and 0.
        /// </summary>
        [TestMethod]
        public void AvarageMark_1and0_return05()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 1);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 0);
            double expected = 0.5;
            // Act
            float actual = Student.AverageMark(x, 2);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method plus works correctly with strings of two object.
        /// </summary>
        [TestMethod]
        public void Plus_x1andx2_return_IvanovPetrov()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 9);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 8);
            string expected = "IvanovPetrov";
            // Act
            string actual = x[0].OutputLastName() + x[1].OutputLastName();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method plus works correctly with values of 3 and 0.
        /// </summary>
        [TestMethod]
        public void Plus_3and0_return3()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 3);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 0);
            float expected = 3;
            // Act
            float actual = x[0].OutputGrade() + x[1].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method plus works correctly with strings of four object.
        /// </summary>
        [TestMethod]
        public void PLus_x0andx1andx2andx3_returnIvanovPetrovAlekseevSemenova()
        {
            // Arrange
            Student[] x = new Student[4];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 1);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 0);
            x[2] = new Student("Alekseev", "Misha", "Pavlovich", 1);
            x[3] = new Student("Semenova", "Alina", "Sergeevna", 1);
            string expected = "IvanovPetrovAlekseevSemenova";
            // Act
            string actual = x[0].OutputLastName() + x[1].OutputLastName() + x[2].OutputLastName() + x[3].OutputLastName();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method plus works correctly with value of 3, 8 and 7.
        /// </summary>
        [TestMethod]
        public void Plus_3and8and7_return18()
        {
            // Arrange
            Student[] x = new Student[3];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 3);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 8);
            x[2] = new Student("Vasnimin", "Sergey", "Petrovich", 7);
            float expected = 18;
            // Act
            float actual = x[0].OutputGrade() + x[1].OutputGrade() + x[2].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method plus works correctly with value of 10, 0 and 0.
        /// </summary>
        [TestMethod]
        public void Plus_10and0and0_return18()
        {
            // Arrange
            Student[] x = new Student[3];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 10);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 0);
            x[2] = new Student("Vasnimin", "Sergey", "Petrovich", 0);
            float expected = 10;
            // Act
            float actual = x[0].OutputGrade() + x[1].OutputGrade() + x[2].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method minus works correctly with value of 8 and 6.
        /// </summary>
        [TestMethod]
        public void Minus_8and6_return3()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 8);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 6);
            float expected = 2;
            // Act
            float actual = x[0].OutputGrade() - x[1].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method minus works correctly with value of 0 and 5.
        /// </summary>
        [TestMethod]
        public void Minus_0and5_return3()
        {
            // Arrange
            Student[] x = new Student[2];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 0);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 5);
            float expected = -5;
            // Act
            float actual = x[0].OutputGrade() - x[1].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method minus works correctly with value of 10, 5 and 2.
        /// </summary>
        [TestMethod]
        public void Minus_10and5and2_return18()
        {
            // Arrange
            Student[] x = new Student[3];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 10);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 5);
            x[2] = new Student("Vasnimin", "Sergey", "Petrovich", 2);
            float expected = 3;
            // Act
            float actual = x[0].OutputGrade() - x[1].OutputGrade() - x[2].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method minus works correctly with value of 0, 3 and 2.
        /// </summary>
        [TestMethod]
        public void Minus_0and3and2_return5()
        {
            // Arrange
            Student[] x = new Student[3];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 0);
            x[1] = new Student("Petrov", "Anton", "Antonovich", 3);
            x[2] = new Student("Vasnimin", "Sergey", "Petrovich", 2);
            float expected = -5;
            // Act
            float actual = x[0].OutputGrade() - x[1].OutputGrade() - x[2].OutputGrade();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// This test checks if the operator overload method minus works correctly with strings of two object.
        /// </summary>
        [TestMethod]
        public void Minus_x0andx1_returnIvanovPetrov()
        {
            // Arrange
            Student[] x = new Student[4];
            x[0] = new Student("Ivanov", "Ivan", "Ivanovich", 10);
            x[1] = new Student("Petrov", "Ivan", "Trofivov", 2);
            string expected = "IvanovPetrov";
            // Act
            string actual = x[0].OutputLastName() + x[1].OutputLastName();
            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
