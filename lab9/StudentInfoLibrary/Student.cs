﻿using System;
using ExceptionLibrary;

namespace StudentInfoLibrary
{
    /// <summary>
    /// класс Student.
    /// Содержит методы работы с типом учащегося.
    /// </summary>
    public class Student
    {
        string LastName;
        string Name;
        string Patronymic;
        float ExamGrade;

        /// <summary>
        /// Метод, создающий объект класса-конструктор.
        /// </summary>
        /// <param name="LastName">Строковая переменная. Фамилия студента.</param>
        /// <param name="Name">Строковая переменная. имя студента.</param>
        /// <param name="Patronymic">Строковая переменная. отчество студента.</param>
        /// <param name="ExamGrade">A float varible. Экзаменационная оценка студента.</param>
        public Student(string LastName, string Name, string Patronymic, float ExamGrade)
        {
            this.LastName = LastName;
            this.Name = Name;
            this.Patronymic = Patronymic;
            this.ExamGrade = ExamGrade;
        }

        /// <summary>
        /// конструктор.
        /// </summary>
        public Student()
        { }


        /// <summary>
        /// Этот метод создает строку для перечисления студентов.
        /// </summary>
        /// <returns>Строка для списка.</returns>
        public string OutputList()
        {
            if((LastName != "") && (Name != "") && (Patronymic != ""))
                return LastName + " " + Name + " " + Patronymic + " Оценка: " + ExamGrade;
            else
                throw new EmptyLineException();
        }


        /// <summary>
        /// Способ вывода строки для оператора плюс и минус в основной программе.
        /// </summary>
        /// <returns>Строка с именем, фамилией, отчеством и знаком.</returns>
        public string Output()
        {
            return LastName + Name + Patronymic + "Оценка: " + ExamGrade;
        }


        /// <summary>
        /// Метод использования экзаменационной оценки в модульных тестах.
        /// </summary>
        /// <returns>The float number.</returns>
        public float OutputGrade()
        {
            return ExamGrade;
        }

        /// <summary>
        /// Свойство замена метода
        /// </summary>
        public float GetExamGrade { get; set; }


        /// <summary>
        ///Метод для использования фамилии в модульном тесте.
        /// </summary>
        /// <returns>строка(фамилия).</returns>
        public string OutputLastName()
        {
            return LastName;
        }


        /// <summary>
        /// Перегрузка оператора методом минус.
        /// </summary>
        /// <param name="c1">объект Student class</param>
        /// <param name="c2">объект of Student class</param>
        /// <returns>новый объект.</returns>
        public static Student operator -(Student c1, Student c2)
        {
            Student student = new Student();
            // Проверьте, совпадают ли имена, фамилии и отчества.
            if (!String.Equals(c1.LastName, c2.LastName) || !String.Equals(c1.Name, c2.Name) || !String.Equals(c1.Patronymic, c2.Patronymic))
            {
                student.LastName = c1.LastName + " " + c2.LastName + "\n";
                student.Name = c1.Name + " " + c2.Name + "\n";
                student.Patronymic = c1.Patronymic + " " + c2.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade - c2.ExamGrade;
                return student;
            }
            else
            {
                student.LastName = c1.LastName + "\n";
                student.Name = c1.Name + "\n";
                student.Patronymic = c1.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade;
                return student;
            }
        }

        /// <summary>
        /// Метод, который учитывает среднюю оценку всех студентов.
        /// </summary>
        /// <param name="c1">Массив объектов класса учащегося.</param>
        /// <returns>The float number средняя оценка.</returns>
        static public float AverageMark(Student[] c1, int amount)
        {
            float Sum = 0;
            // Цикл суммировал всю отметку.
            for (int i = 0; i < amount; i++)
            {
                Sum += c1[i].ExamGrade;
            }
            Sum = Sum / amount;
            return Sum;
        }

        /// <summary>
        /// Оператор перегрузка метод плюс.
        /// </summary>
        /// <param name="c1">объект of Student class.</param>
        /// <param name="c2">объект of Student class.</param>
        /// <returns>New object.</returns>
        public static Student operator +(Student c1, Student c2)
        {
            // Создайте новый объект класса Student.
            Student student = new Student();
            // Проверьте, совпадают ли имена, фамилии и отчества.
            if (!String.Equals(c1.LastName, c2.LastName) || !String.Equals(c1.Name, c2.Name) || !String.Equals(c1.Patronymic, c2.Patronymic))
            {
                student.LastName = c1.LastName + " " + c2.LastName + "\n";
                student.Name = c1.Name + " " + c2.Name + "\n";
                student.Patronymic = c1.Patronymic + " " + c2.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade + c2.ExamGrade;
                return student;
            }
            else
            {
                student.LastName = c1.LastName + "\n";
                student.Name = c1.Name + "\n";
                student.Patronymic = c1.Patronymic + "\n";
                student.ExamGrade = c1.ExamGrade;
                return student;
            }
        }
    }
}
