﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionLibrary
{
    public class InvalidAgrumentException : Exception
    {
        public InvalidAgrumentException() : base ("Некорректный ввод оценки.")
        {
        }
    }
}
