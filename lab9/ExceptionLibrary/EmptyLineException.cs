﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionLibrary
{
    public class EmptyLineException : Exception
    {
        public EmptyLineException() : base("Ввод не осуществлен.")
        {
            
        }

    }
}
