﻿using System;
using System.Windows;
using StudentInfoLibrary;
using ExceptionLibrary;

namespace InformationWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int amountStudents = 0;
        Student[] students = new Student[1000];

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddRecords()
        {
            int grades = 0;
            if (Int32.TryParse(gradeBox.Text, out int grade))
            {
                grades = grade;
                if ((grades > 10) || (grades <= 0))
                {
                    throw new InvalidAgrumentException();
                }
            }
            if ((lastNameBox.Text != "") && (firstNameBox.Text != "") && (patronymicBox.Text != ""))
            {
                students[amountStudents] = new Student(lastNameBox.Text, firstNameBox.Text, patronymicBox.Text, grades);
                amountStudents++;
            }
            else
                throw new EmptyLineException();
        }


        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddRecords();
                lastNameBox.Clear();
                firstNameBox.Clear();
                patronymicBox.Clear();
                gradeBox.Clear();
            }
            catch(EmptyLineException exp)
            {
                MessageBox.Show("Ввод не осуществлен." + exp.Message );
            }
            catch(InvalidAgrumentException)
            {
                MessageBox.Show("Некорректный ввод оценки.");
            }
        }

        private void Output_Click(object sender, RoutedEventArgs e)
        {
            string text = "";
            try
            {
                for (int i = 0; i < amountStudents; i++)
                {
                    text += i + 1 + ". " + students[i].OutputList() + "\n";
                }
                textBox1.Text = text;
            }
            catch
            {
                MessageBox.Show("Некорректный ввод.");
            }
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            Student sum;
            if((Int32.TryParse(plus1.Text, out int element1)) && (Int32.TryParse(plus2.Text, out int element2)))
            {
                sum = students[element1 - 1] + students[element2 - 1];
                textBox1.Text = sum.Output();
            }
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            Student sum;
            if ((Int32.TryParse(plus1.Text, out int element1)) && (Int32.TryParse(plus2.Text, out int element2)))
            {
                sum = students[element1 - 1] - students[element2 - 1];
                textBox1.Text = sum.Output();
            }
        }

        private void AverageGrade_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                textBox1.Text = $"{Student.AverageMark(students, amountStudents)}";
            }
            catch(DivideByZeroException)
            {
                MessageBox.Show("Деление на нуль невозможно.");
            }
        }
    }
}
