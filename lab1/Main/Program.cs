﻿using FigureLibrary;
using System;

namespace TrapezeApp
{
    class Program
    {
        static void Main(string[] args) //пустой , ничего не возвращает для всего класса
        {
            double x1 = 0, x2 = 0;  //задаем начальные значения
            bool flag = false;
            while (flag == false)
            {
                Console.WriteLine("Введите х1");
                x1 = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите х2");
                x2 = Convert.ToDouble(Console.ReadLine());
                flag = Trapeze.IsFigureExist(x1, x2);
                if (flag == true)
                    Console.WriteLine("Фигура существует");
                else
                    Console.WriteLine("Фигура не существует, введите значение ещё раз");
            }
            Trapeze trapeze = new Trapeze(x1, x2);
            flag = true;
            while (flag)
            {
                Console.WriteLine("1.Вычислить длину сторон трапеции");
                Console.WriteLine("2.Вычислить периметр трапеции");
                Console.WriteLine("3.Вычислить площадь трапеции");
                Console.WriteLine("4.Проверить принадлежность точки");
                Console.WriteLine("Нажмите любую кнопку чтобы");
                int n;
                n = Convert.ToInt32(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        double l1, l2, l3, l4;
                        l1 = trapeze.FindSide1();
                        Console.WriteLine("Первая сторонa " + Convert.ToString(l1));
                        l2 = trapeze.FindSide2();
                        Console.WriteLine("Вторая сторона " + Convert.ToString(l2));
                        l3 = trapeze.FindSide3();
                        Console.WriteLine("Третья сторона " + Convert.ToString(l3));
                        l4 = trapeze.FindSide4();
                        Console.WriteLine("Четвёртая сторона " + Convert.ToString(l4));
                        break;
                    case 2:
                        double perimetr;
                        perimetr = trapeze.FindPerimetr();
                        Console.WriteLine("Периметр = " + Convert.ToString(perimetr));
                        break;
                    case 3:
                        double square;
                        square = trapeze.FindSquare();
                        Console.WriteLine("Площадь = " + Convert.ToString(square));
                        break;
                    case 4:
                        Console.WriteLine("Введите координаты точки");
                        double x, y;
                        Console.WriteLine("Введите координату х");
                        x = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Введите координату y");
                        y = Convert.ToDouble(Console.ReadLine());
                        if (trapeze.IsPointBelong(x, y))
                            Console.WriteLine("Точка с координатами " + Convert.ToString(x) + ";" + Convert.ToString(y) + " принадлежит трапеции");
                        else
                            Console.WriteLine("Точка с координатами " + Convert.ToString(x) + ";" + Convert.ToString(y) + " не принадлежит трапеции");
                        break;
                    default:
                        flag = false;
                        break;

                }
            }
            Console.ReadKey(); // чтобы консольное окно не закрывалось
        }
    }
}