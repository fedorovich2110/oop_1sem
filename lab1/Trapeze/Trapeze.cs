﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureLibrary
{
    /// <summary>
    /// Класс для работы с криволинейной трапецией.
    /// </summary>
    public class Trapeze
    {
        double x1, x2;
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
		public Trapeze(double x1, double x2) //конструктор котор нужен для создания объекта
        {
            this.x1 = x1; // берет внешнюю переменную и записывает в неё внутреннюю
            this.x2 = x2;
        }
        /// <summary>
        /// Находим площадь.
        /// </summary>
        /// <returns>Возвращаем double.</returns>
		public double FindSquare()
        {
            return (Math.Pow(x2, 3) / 3) - (Math.Pow(x1, 3) / 3);
        }
        /// <summary>
        /// Метод по нахождению третьей стороны. Реккурентное нахождение приблизительного значения длины интеграла функции х^2
        /// </summary>
        /// <returns>Возвращаем переменную double. третья сторона.</returns>
		public double Find3rdLen() //нахождение третей стороны
        {
            double lenght = 0;
            double dx = (x2 - x1) / 100;
            double startX = x1;
            double finishX = x1 + dx;
            while (finishX < x2)
            {
                lenght += Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(finishX - startX, 2));
                startX += dx;
                finishX += dx;
            }
            return lenght;
        }
        /// <summary>
        /// Метод по вычислению 1 стороны трапеции.
        /// </summary>
		public double FindSide1()
        {
            return x2 - x1;
        }
        /// <summary>
        /// метод по вычислению 2 стороны трапеции
        /// </summary>
        /// <returns></returns>
        public double FindSide2()
        {
            return Math.Pow(x1, 2);
        }
        /// <summary>
        /// метод по вычислению 3 стороны трапеции
        /// </summary>
        /// <returns></returns>
        public double FindSide3()
        {
            return Find3rdLen();
        }

        /// <summary>
        /// метод по вычислению 4 стороны
        /// </summary>
        /// <returns></returns>
        public double FindSide4() //пустой , ничего не возвращает
        {
            return Math.Pow(x2, 2);
        }

        /// <summary>
        /// Находим периметр.
        /// </summary>
        /// <returns>Возвращаем double.</returns>
        public double FindPerimetr()
        {
            return FindSide1() + FindSide2() + FindSide3() + FindSide4();
        }
        /// <summary>
        /// Проверяет фигуру на существование.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns>Возвращаем bool.</returns>
		public static bool IsFigureExist(double x1, double x2) // true false
        {
            if (x1 * x2 > 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Принадлежность точки.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Возвращает bool.</returns>
		public bool IsPointBelong(double x, double y)
        {
            if (y < Math.Pow(x, 2) && x >= x1 && x <= x2 && y != 0)
                return true;
            else
                return false;
        }
    }
}